package ru.t1.strelcov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.dto.model.TaskDTO;

@Setter
@Getter
@NoArgsConstructor
public final class TaskRemoveByNameResponse extends AbstractTaskResponse {

    public TaskRemoveByNameResponse(@NotNull final TaskDTO task) {
        super(task);
    }

}
