package ru.t1.strelcov.tm.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.enumerated.EntityEventType;

import java.util.Date;

@Getter
@Setter
public class EntityEvent {

    @NotNull
    private final Object entity;

    @NotNull
    private final EntityEventType type;

    @NotNull
    private final String table;

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private final Date date = new Date();

    public EntityEvent(
            @NotNull final EntityEventType type,
            @NotNull final Object entity,
            @NotNull final String table
    ) {
        this.entity = entity;
        this.type = type;
        this.table = table;
    }

}
