package ru.t1.strelcov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Setter
@Getter
@NoArgsConstructor
public final class ServerAboutResponse extends AbstractResultResponse {

    @Nullable
    private String email;

    @Nullable
    private String name;

    public ServerAboutResponse(@Nullable final String email, @Nullable final String name) {
        this.email = email;
        this.name = name;
    }

}
