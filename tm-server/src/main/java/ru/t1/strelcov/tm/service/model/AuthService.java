package ru.t1.strelcov.tm.service.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.service.IPropertyService;
import ru.t1.strelcov.tm.api.service.model.IAuthService;
import ru.t1.strelcov.tm.api.service.model.IUserService;
import ru.t1.strelcov.tm.exception.empty.EmptyLoginException;
import ru.t1.strelcov.tm.exception.empty.EmptyPasswordException;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;
import ru.t1.strelcov.tm.exception.entity.IncorrectPasswordException;
import ru.t1.strelcov.tm.exception.entity.UserLockedException;
import ru.t1.strelcov.tm.exception.entity.UserLoginExpiredException;
import ru.t1.strelcov.tm.model.Session;
import ru.t1.strelcov.tm.model.User;
import ru.t1.strelcov.tm.util.CryptUtil;
import ru.t1.strelcov.tm.util.HashUtil;

import java.util.Date;
import java.util.Optional;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    public AuthService(@NotNull final IUserService userService, @NotNull IPropertyService propertyService) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public String login(@Nullable String login, @Nullable String password) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).filter((i) -> !i.isEmpty()).orElseThrow(EmptyPasswordException::new);
        @NotNull final User user = userService.findByLogin(login);
        @NotNull final String passwordHash = HashUtil.salt(propertyService.getPasswordSecret(), propertyService.getPasswordIteration(), password);
        if (!user.getPasswordHash().equals(passwordHash)) throw new IncorrectPasswordException();
        if (user.getLock()) throw new UserLockedException();
        return getToken(user);
    }

    @Override
    public void logout(@Nullable String token) {
    }

    @SneakyThrows
    @NotNull
    @Override
    public Session validateToken(@Nullable String token) {
        Optional.ofNullable(token).filter((i) -> !i.isEmpty()).orElseThrow(AccessDeniedException::new);
        try {
            @NotNull final String sessionKey = propertyService.getSessionSecret();
            @NotNull final String json = CryptUtil.decrypt(token, sessionKey);
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final Session session = objectMapper.readValue(json, Session.class);
            if (session.getDate() == null) throw new AccessDeniedException();
            final long delta = ((new Date()).getTime() - session.getDate().getTime()) / 1000 / 60;
            if (propertyService.getSessionTimeout() < delta) throw new UserLoginExpiredException();
            return session;
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException(e.getClass() + ": " + e.getMessage());
        }
    }

    @SneakyThrows
    @NotNull
    private String getToken(@NotNull User user) {
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setRole(user.getRole());
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionSecret();
        return CryptUtil.encrypt(json, sessionKey);
    }

}
