package ru.t1.strelcov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.repository.dto.IDTORepository;
import ru.t1.strelcov.tm.api.service.IConnectionService;
import ru.t1.strelcov.tm.api.service.dto.IDTOService;
import ru.t1.strelcov.tm.dto.model.AbstractEntityDTO;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.entity.EntityNotFoundException;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractDTOService<E extends AbstractEntityDTO> implements IDTOService<E> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractDTOService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    public abstract IDTORepository<E> getRepository(@NotNull final EntityManager entityManager);

    @SneakyThrows
    @NotNull
    @Override
    public List<E> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDTORepository<E> repository = getRepository(entityManager);
        try {
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void add(@Nullable final E entity) {
        if (entity == null) return;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDTORepository<E> repository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.add(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void addAll(@Nullable final List<E> list) {
        @Nullable List<E> listNonNull = Optional.ofNullable(list).map(l -> l.stream().filter(Objects::nonNull).collect(Collectors.toList())).orElse(null);
        if (listNonNull == null || listNonNull.size() == 0) return;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDTORepository<E> repository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.addAll(listNonNull);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDTORepository<E> repository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @NotNull
    @Override
    public E findById(@Nullable final String id) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDTORepository<E> repository = getRepository(entityManager);
        try {
            return Optional.ofNullable(repository.findById(id)).orElseThrow(EntityNotFoundException::new);
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @NotNull
    @Override
    public E removeById(@Nullable final String id) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDTORepository<E> repository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            final E entity = Optional.ofNullable(repository.removeById(id)).orElseThrow(EntityNotFoundException::new);
            entityManager.getTransaction().commit();
            return entity;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
