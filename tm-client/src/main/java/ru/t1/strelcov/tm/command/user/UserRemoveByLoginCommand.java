package ru.t1.strelcov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.endpoint.IUserEndpoint;
import ru.t1.strelcov.tm.dto.request.UserRemoveByLoginRequest;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.util.TerminalUtil;

public final class UserRemoveByLoginCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "user-remove-by-login";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove user by login.";
    }

    @Override
    public void execute() {
        @NotNull final IUserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        userEndpoint.removeUserByLogin(new UserRemoveByLoginRequest(getToken(), login));
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
