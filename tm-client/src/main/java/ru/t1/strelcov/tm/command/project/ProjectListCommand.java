package ru.t1.strelcov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.strelcov.tm.dto.model.ProjectDTO;
import ru.t1.strelcov.tm.dto.request.ProjectListRequest;
import ru.t1.strelcov.tm.dto.request.ProjectListSortedRequest;
import ru.t1.strelcov.tm.enumerated.SortType;
import ru.t1.strelcov.tm.exception.system.IncorrectSortOptionException;
import ru.t1.strelcov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @NotNull
    @Override
    public String description() {
        return "List projects.";
    }

    @Override
    public void execute() {
        @NotNull final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT FIELD " + Arrays.toString(SortType.values()) + ":");
        @NotNull List<ProjectDTO> projects;
        final String sort = TerminalUtil.nextLine();
        if (sort.isEmpty())
            projects = projectEndpoint.listProject(new ProjectListRequest(getToken())).getList();
        else {
            if (SortType.isValidByName(sort)) {
                @NotNull final SortType sortType = SortType.valueOf(sort);
                projects = projectEndpoint.listSortedProject(new ProjectListSortedRequest(getToken(), sort)).getList();
                System.out.println(sortType.getDisplayName());
            } else
                throw new IncorrectSortOptionException(sort);
        }
        int index = 1;
        for (@NotNull final ProjectDTO project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
    }

}
