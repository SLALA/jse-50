package ru.t1.strelcov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.strelcov.tm.dto.model.UserDTO;
import ru.t1.strelcov.tm.dto.request.UserProfileRequest;

public final class UserShowProfileCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "user-profile";
    }

    @NotNull
    @Override
    public String description() {
        return "Display the current user profile data.";
    }

    @Override
    public void execute() {
        @NotNull final IAuthEndpoint authEndpoint = serviceLocator.getAuthEndpoint();
        System.out.println("[PROFILE]");
        @NotNull final UserDTO user = authEndpoint.getProfile(new UserProfileRequest(getToken())).getUser();
        showUser(user);
    }

}
