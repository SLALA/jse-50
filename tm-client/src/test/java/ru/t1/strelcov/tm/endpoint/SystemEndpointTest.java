package ru.t1.strelcov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.strelcov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.strelcov.tm.api.service.IPropertyService;
import ru.t1.strelcov.tm.dto.request.ServerAboutRequest;
import ru.t1.strelcov.tm.dto.request.ServerVersionRequest;
import ru.t1.strelcov.tm.marker.IntegrationCategory;
import ru.t1.strelcov.tm.service.PropertyService;

public class SystemEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final String host = propertyService.getServerHost();

    @NotNull
    private final Integer port = propertyService.getServerPort();

    @NotNull
    private final ISystemEndpoint systemEndpoint = ISystemEndpoint.newInstance(host, port);

    @Nullable
    private String token;

    @Before
    public void before() {
    }

    @After
    public void after() {
    }

    @Category(IntegrationCategory.class)
    @Test
    public void aboutTest() {
        Assert.assertNotNull(systemEndpoint.getAbout(new ServerAboutRequest()).getName());
    }

    @Category(IntegrationCategory.class)
    @Test
    public void versionTest() {
        Assert.assertNotNull(systemEndpoint.getVersion(new ServerVersionRequest()).getVersion());
    }

}
